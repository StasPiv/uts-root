<?php

namespace Playzone\PlayzoneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PlayzonePlayzoneBundle:Default:index.html.twig');
    }
}
