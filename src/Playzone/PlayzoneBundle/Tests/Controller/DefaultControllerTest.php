<?php

namespace Playzone\PlayzoneBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/playzone/Fabien');

        $this->assertTrue($crawler->filter('html:contains("Playzone Fabien")')->count() > 0);
    }
}
